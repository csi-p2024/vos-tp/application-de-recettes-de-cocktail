import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const FavoryScreen = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Favory</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'rgb(0, 185, 205)',
    },
});

export default FavoryScreen;