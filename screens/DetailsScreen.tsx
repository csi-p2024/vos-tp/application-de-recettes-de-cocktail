import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const DetailsScreen = ({ route }) => {
    const { cocktail } = route.params;

    return (
        <View style={styles.container}>
            <Text style={styles.title}>{cocktail.strDrink}</Text>
            <Image source={{ uri: cocktail.strDrinkThumb }} style={styles.image} />
            <Text style={styles.description}>{cocktail.strInstructions}</Text>
            <View style={styles.separator} />
            <Text style={styles.ingredientsTitle}>Ingredients:</Text>
            {cocktail.strIngredients && cocktail.strIngredients.map((ingredient, index) => (
                <Text key={index} style={styles.ingredient}>{ingredient}</Text>
            ))}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginVertical: 20,
        color: 'rgb(0, 185, 205)',
    },
    image: {
        width: '100%',
        height: 200,
    },
    description: {
        fontSize: 16,
        textAlign: 'center',
        marginVertical: 20,
        color: 'rgb(0, 185, 205)',
    },
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: '#000',
        marginVertical: 20,
    },
    ingredientsTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'rgb(0, 185, 205)',
    },
    ingredient: {
        fontSize: 16,
        textAlign: 'center',
    },
});

export default DetailsScreen;