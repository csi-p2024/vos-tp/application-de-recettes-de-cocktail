import React, {useEffect, useState} from 'react';
import {View, Text, Image, StyleSheet, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Loader from '../composants/Loader';

const numColumns = 3;
const size = Dimensions.get('window').width / numColumns;

const SearchByIngredientScreen = ({navigation}) => {
    const [cocktails, setCocktails] = useState([]);
    const [ingredient, setIngredient] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const loadCocktails = () => {
        if (!ingredient) {
            return;
        }
        setIsLoading(true);
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${ingredient}`)
            .then(response => response.json())
            .then(data => {
                setCocktails(data.drinks);
                setIsLoading(false);
            })
            .catch(error => {
                console.error(error);
                setIsLoading(false);
            });
    };

    const loadIngredients = () => {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list`)
            .then(response => response.json())
            .then(data => {
                const sortedIngredients = data.drinks.map(drink => ({ id: drink.strIngredient1, name: drink.strIngredient1 }));
                sortedIngredients.sort((a, b) => a.name.localeCompare(b.name));
                setIngredients(sortedIngredients);
            })
            .catch(error => console.error(error));
    };

    useEffect(() => {
        loadCocktails();
        loadIngredients();
    }, [ingredient]);

    const renderItem = ({item}) => (
        <TouchableOpacity
            style={styles.itemContainer}
            onPress={() => navigation.navigate('Details', {cocktail: item})}
        >
            <Image source={{uri: item.strDrinkThumb}} style={styles.image}/>
            <Text style={styles.title}>{item.strDrink}</Text>
        </TouchableOpacity>
    );

    return (
        <View style={styles.container}>
            <Picker
                selectedValue={ingredient}
                onValueChange={(itemValue) => setIngredient(itemValue)}
            >
                {ingredients.map((item) => (
                    <Picker.Item key={item.id} label={item.name} value={item.id}/>
                ))}
            </Picker>
            {isLoading ? <Loader/> : (
                <FlatList
                    data={cocktails}
                    keyExtractor={(item) => item.idDrink}
                    renderItem={renderItem}
                    numColumns={numColumns}
                    key={numColumns}
                />
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 20,
    },
    itemContainer: {
        width: size,
        height: size,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 1,
        marginBottom: 20,
    },
    image: {
        width: '100%',
        height: '70%',
    },
    title: {
        fontSize: 16 * 1.1,
        textAlign: 'center',
        padding: 10,
        color: 'rgb(0, 185, 205)',
    },
    globalTitle: {
        fontSize: 16 * 1.2,
        fontWeight: 'bold',
        color: 'rgb(0, 185, 205)',
        marginBottom: 20,
        textAlign: 'center',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        paddingLeft: 10,
        marginBottom: 20,
    },
});
export default SearchByIngredientScreen;