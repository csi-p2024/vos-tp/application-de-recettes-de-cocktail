import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import Loader from '../composants/Loader';
const numColumns = 3;
const size = Dimensions.get('window').width/numColumns;

const HomeScreen = ({ navigation }) => {
    const [cocktails, setCocktails] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const loadCocktails = () => {
        setIsLoading(true);
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic`)
            .then(response => {
                return response.json();
            })
            .then(data => {
                setCocktails(data.drinks);
                setIsLoading(false);
            })
    };


    useEffect(() => {
        loadCocktails();
    }, []);

    const renderItem = ({ item }) => (
        <TouchableOpacity
            style={styles.itemContainer}
            onPress={() => navigation.navigate('Details', { cocktail: item })}
        >
            <Image source={{ uri: item.strDrinkThumb }} style={styles.image} />
            <Text style={styles.title}>{item.strDrink}</Text>
        </TouchableOpacity>
    );

    return (
        <View style={styles.container}>
            <Text style={styles.globalTitle}>Some cocktails ( just for you )</Text>
            {isLoading ? <Loader /> : (
                <FlatList
                    data={cocktails}
                    keyExtractor={(item) => item.idDrink}
                    renderItem={renderItem}
                    numColumns={numColumns}
                    key={numColumns}
                />
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 20,
    },
    itemContainer: {
        width: size,
        height: size,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 1,
        marginBottom: 20,
    },
    image: {
        width: '100%',
        height: '70%',
    },
    title: {
        fontSize: 16 * 1.1,
        textAlign: 'center',
        padding: 10,
        color: 'rgb(0, 185, 205)',
    },
    globalTitle: {
        fontSize: 16 * 1.2,
        fontWeight: 'bold',
        color: 'rgb(0, 185, 205)',
        marginBottom: 20,
        textAlign: 'center',
    },
});
export default HomeScreen;