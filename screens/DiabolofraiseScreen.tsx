import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';

const DiaboloFraiseScreen = () => {
    return (
        <ImageBackground source={require('../img/generated_video.gif')} style={styles.backgroundImage}>
            <View style={styles.container}>
                <Text style={styles.title}>Diabolo Fraise</Text>
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: 'rgb(0, 185, 205)',
    },
    image: {
        width: '100%',
        height: 200,
    },
});

export default DiaboloFraiseScreen;