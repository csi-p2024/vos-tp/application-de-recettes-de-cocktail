import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

import HomeScreen from './screens/HomeScreen';
import SearchByIngredientScreen from './screens/SearchByIngredientScreen';
import DiabolofraiseScreen from './screens/DiabolofraiseScreen';
import FavoryScreen from './screens/FavoryScreen';

const Tab = createBottomTabNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Tab.Navigator initialRouteName="Home">
                <Tab.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                        title: 'Home',
                        tabBarIcon: ({ color, size }) => (
                            <Icon name="home" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Recherche par ingrédient"
                    component={SearchByIngredientScreen}
                    options={{
                        title: 'Recherche par ingrédient',
                        tabBarIcon: ({ color, size }) => (
                            <Icon name="search" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Diabolofraise"
                    component={DiabolofraiseScreen}
                    options={{
                        title: 'Diabolofraise',
                        tabBarIcon: ({ color, size }) => (
                            <Icon name="glass" color={color} size={size} />
                        ),
                    }}
                />
                <Tab.Navigator initialRouteName="Home">
                    {/* ... autres routes ... */}
                    <Tab.Screen
                        name="Favory"
                        component={FavoryScreen}
                        options={{
                            title: 'Favory',
                            tabBarIcon: ({ color, size }) => (
                                <Icon name="star" color={color} size={size} />
                            ),
                        }}
                    />
                </Tab.Navigator>
            </Tab.Navigator>
        </NavigationContainer>
    );
}